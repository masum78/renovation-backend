﻿
namespace common.enums
{
    public enum SortType
    {
        Ascending, Descending
    }
}
