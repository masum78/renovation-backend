﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("HouseType")]
   public class HouseType
    {
        public int HouseTypeId { get; set; }
        public string HouseTypeName { get; set; }
        public string FileName { get; set; }
    }
}
