﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class UserRule
    {
        public int UserRuleId { get; set; }
        public string UserRuleName { get; set; }
    }
}
