﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace dal.models
{
    [Table("ItemCategory")]
    public class ItemCategory
    {
        public int ItemCategoryId { get; set; }
        public string ItemCategoryName { get; set; }
        public int ParentId { get; set; }
        public virtual ICollection<ItemCategory> Children { get; set; }
    }
}
