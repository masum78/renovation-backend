﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class UserTask
    {
        public int UserTaskId { get; set; }
        public string UserTaskName { get; set; }
    }
}
