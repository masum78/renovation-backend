﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class Menu1
    {
        public int Menu1Id { get; set; }
        public string Title { get; set; }
        public string Translate { get; set; }
        public string Type { get; set; }
        public string Icon { get; set; }
        //public ICollection<SubMenu> Children { get; set; }
        public int SubMenuId { get; set; }
        public SubMenu1 SubMenu1 { get; set; }
    }
}
