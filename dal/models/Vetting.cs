﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class Vetting
    {
        public int VettingId { get; set; }
        public int TradeServiceId { get; set; }
        public TradeService TradeService { get; set; }
        public string VettingHead { get; set; }
        public string VettingValueofHead { get; set; }

        public int Status { get; set; }
        public string Comments { get; set; }

    }
}
