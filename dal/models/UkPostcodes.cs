﻿

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace dal.models
{
    [Table("UkPostcodes")]
    public class UkPostcodes
    {
        [Key]
        public int PostcodeId { get; set; }
        public string pcd { get; set; }
        public string pcd2 { get; set; }
        public string pcds { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }

        public DateTime DateCreated
        {
            get
            {
                return this.dateCreated.HasValue
                   ? this.dateCreated.Value
                   : DateTime.Now;
            }

            set { this.dateCreated = value; }
        }

        private DateTime? dateCreated = null;
    }
}
