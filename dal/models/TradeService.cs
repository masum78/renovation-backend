﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class TradeService
    {
        public int TradeServiceId { get; set; }
        public int CompanyProfileId { get; set; }
        public CompanyProfile CompanyProfile { get; set; }
        public int ItemCategoryId { get; set; }
        public ItemCategory ItemCategory { get; set; }
        public string ServiceDescription { get; set; }
        public string CellNo { get; set; }
        public bool IsChecked { get; set; }
        public bool IsApproved { get; set; }

    }
}
