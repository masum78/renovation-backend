﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class UserRuleMapper
    {
        public int UserRuleMapperId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int UserRuleId { get; set; }
        public UserRule UserRule { get; set; }
    }
}
