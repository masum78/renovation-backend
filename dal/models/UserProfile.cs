﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class UserProfile
    {
        public int UserProfileId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string PostCode { get; set; }
        public string ProfilePicture { get; set; }
        public string TradeLicence { get; set; }
        public string MobilNo { get; set; }
        public string Address { get; set; }
        public ICollection<AdviceCenter> AdviceCenter { get; set; }
        public ICollection<Tradesman> Tradesman { get; set; }
    }
}
