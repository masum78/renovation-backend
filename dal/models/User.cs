﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("User")]
    public class User
    {
        
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool? Tradesman { get; set; }
        public DateTime? CreatedDate { get; set; } = DateTime.Now;
        public ICollection<UserProfile> UserProfile { get; set; }
        public virtual ICollection<UserFeedback> Books { get; set; }
        public bool IsActive { get; set; }
    }
}
