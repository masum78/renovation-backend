﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace dal.models
{
    [Table("UserFeedback")]
    public class UserFeedback
    {
        public int UserFeedbackId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int TradesmanId { get; set; }
        public Tradesman Tradesman { get; set; }
        public string Comments { get; set; }
        public int RatingPoint { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
