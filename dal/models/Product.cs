﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("Products")]
   public class Product
    {
        public int ProductId { get; set; }
        public string ProductDescription { get; set; }
        public int MaterialId { get; set; }
        public Material Material { get; set; }
        public string ProductName { get; set; }
        public string ProductUrl { get; set; }
        public string ImageUrl { get; set; }
    }
}
