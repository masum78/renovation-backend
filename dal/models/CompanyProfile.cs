﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class CompanyProfile
    {
        public int CompanyProfileId { get; set; }
        public string CompanyProfileName { get; set; }
        public string CompanyDescription { get; set; }
        public string ImageUrl { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string Accreditations { get; set; }
        public string Insurance { get; set; }
        public string PostCode { get; set; }
        public string CompanyAddress { get; set; }
        public string Identification { get; set; }
        public string LogoPath { get; set; }

    }
}
