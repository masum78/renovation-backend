﻿
using dal.dmo;
using Microsoft.EntityFrameworkCore;
using System;

namespace dal.models
{
   internal class renovationContext : DbContext
    {
        public renovationContext()
        {

        }
        public renovationContext(DbContextOptions<renovationContext> options) 
            :base(options)
        {

        }
        public virtual DbSet<SingleString> SingleString { get; set; }
        public virtual DbSet<SingleInt> SingleInt { get; set; }

        public DbSet<UserRuleMapper> UserRuleMapper { get; set; }
        public DbSet<UserTask> UserTask { get; set; }
        public DbSet<UserRule> UserRule { get; set; }
        //public DbSet<SubMenu1> SubMenu1 { get; set; } 
        //public DbSet<Menu1> Menu1 { get; set; }
        public DbSet<Vetting> Vetting { get; set; }
        public DbSet<TradeService> TradeService { get; set; }
        public DbSet<CompanyProfile> CompanyProfile { get; set; }
        public DbSet<KnowledgeBase> KnowledgeBase { get; set; }
        public DbSet<CostSetup> CostSetup { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<HouseType> HouseType { get; set; }
        public DbSet<HouseTypeLayout> HouseTypeLayout { get; set; }
        public DbSet<HouseBuildingCost> HouseBuildingCost { get; set; }

        public DbSet<UkPostcodes> UkPostcodes { get; set; }
        public DbSet<UserFeedback> UserFeedback { get; set; }
        public DbSet<UserProfile> UserProfile { get; set; }
        public DbSet<AdviceCenter> AdviceCenter { get; set; }
        public DbSet<AdviceCenterPostComment> AdviceCenterPostComment { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Tradesman> Tradesman { get; set; }
        public DbSet<ItemCategory> ItemCategory { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-F2O86RF\\SQLEXPRESS;Database=BuildingRenovation;User Id=sa;Password=masum@123;", builder =>
            {
                builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null);
            });
            base.OnConfiguring(optionsBuilder);

            //if (!optionsBuilder.IsConfigured)
            //{
            //    optionsBuilder.UseSqlServer("Server=DESKTOP-F2O86RF\\SQLEXPRESS;Database=Tradesman;User Id=sa;Password=masum@123;");
            //}
        }

    }
}
