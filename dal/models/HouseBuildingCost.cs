﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("HouseBuildingCost")]
   public class HouseBuildingCost
    {
        public int HouseBuildingCostId { get; set; }
        public int HouseTypeLayoutId { get; set; }
        public HouseTypeLayout HouseTypeLayout { get; set; }
        public string MaterialName { get; set; }
        public float Quantity { get; set; }
        public string Unit { get; set; }
        public float ApproximateCost { get; set; }

    }
}
