﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class SubMenu1
    {
        public int SubMenu1Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public ICollection<Menu1> Menu1 { get; set; }
    }
}
