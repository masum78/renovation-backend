﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dal.models
{
   public class AdviceCenter
    {
        public int AdviceCenterId { get; set; }
        public string Title { get; set; }
        public string PostText { get; set; }
        public string AuthorName { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public string PostImage { get; set; }
        public int UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
        public ICollection<AdviceCenterPostComment> AdviceCenterPostComment { get; set; }
    }
}
