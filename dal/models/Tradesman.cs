﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("Tradesman")]
  public class Tradesman
    {
        public int TradesmanId { get; set; }
        public string TradesmanName { get; set; }
        public int UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
        public int ItemCategoryId { get; set; }
        public ItemCategory ItemCategory { get; set; }
        public string TradeLicence { get; set; }
        public string PostCode { get; set; }
        public string Image { get; set; }
        public string Tags { get; set; }
        //public string Title { get; set; }
        //public string Subtitle { get; set; }
        //public string Features { get; set; }
        //public string LastUpdate { get; set; }
        //public string AddtoCart { get; set; }
        //public string PersonsName { get; set; }
        //public string PersonsAddress { get; set; }
        public string ServiceDescription { get; set; }
        public string MobilNo { get; set; }
        public string CompanyName { get; set; }
        public double Rate { get; set; }
        public bool Active { get; set; }
        public virtual ICollection<UserFeedback> Books { get; set; }

    }
}
