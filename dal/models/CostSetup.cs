﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("CostSetup")]
    public  class CostSetup
    {
        public int CostSetupId { get; set; }
        public int MaterialId { get; set; }
        public Material Material { get; set; }
        public string Quantity { get; set; }
        public string Cost { get; set; }
    }
}
