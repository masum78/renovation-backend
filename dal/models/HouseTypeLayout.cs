﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("HouseTypeLayout")]
   public class HouseTypeLayout
    {
        public int HouseTypeLayoutId { get; set; }
        public string LayoutName { get; set; }
        public int HouseTypeId { get; set; }
        public HouseType HouseType { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public float Width { get; set; }
        public float Length { get; set; }
        public float FloorThickness { get; set; }
        public float WallThickness { get; set; }
        public float WallHeight { get; set; }
        public float NBFloors { get; set; }
        public float RoofHeight { get; set; }
        public float RoofThickness { get; set; }
        public float RoofTopLength { get; set; }
        public float RoofBottomLength { get; set; }

    }
}
