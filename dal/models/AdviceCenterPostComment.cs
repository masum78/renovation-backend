﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace dal.models
{
    [Table("AdviceCenterPostComment")]
    public class AdviceCenterPostComment
    {
        public int AdviceCenterPostCommentId { get; set; }
        public int AdviceCenterId { get; set; }
        public AdviceCenter AdviceCenter { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public bool IsActive { get; set; } = false;
    }
}
