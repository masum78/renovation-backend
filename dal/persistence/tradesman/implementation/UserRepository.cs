﻿using dal.models;
using dal.repositories.user;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dal.persistence.tradesman.implementation
{
    internal class UserRepository : Repository<User>, IUserRepository
    {
        private renovationContext _iContext;
        public UserRepository(renovationContext context) : base(context)
        {
            _iContext = context;
        }

        public renovationContext Context
        {
            get
            {
                return _context as renovationContext;
            }
        }

        public User GetUserById(int userId)
        {
            try
            {
                var users = Context.User.Where(x=>x.UserId == userId).FirstOrDefault();

                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserRule GetUserRuleById(int ruleId)
        {
            try
            {
                var userRule = Context.UserRule.Where(x => x.UserRuleId == ruleId).FirstOrDefault();

                return userRule;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<UserRuleMapper> GetUserRuleMapper()
        {
            try
            {
                var userRuleMapper = from u in Context.User
                                     join m in Context.UserRuleMapper on u.UserId equals m.UserId into uMap
                                     from mm in uMap.DefaultIfEmpty()
                                     join ur in Context.UserRule on mm.UserRuleId equals ur.UserRuleId into uMap2
                                     from p in uMap2.DefaultIfEmpty()
                                     where u.IsActive == true
                                     select new UserRuleMapper
                                     {
                                         User = new User
                                         {
                                             UserId = u.UserId,
                                             UserName = u.UserName
                                         },
                                         UserRule = new UserRule
                                         {
                                             UserRuleId = p.UserRuleId,
                                             UserRuleName = p.UserRuleName
                                         }
                                     };
                return userRuleMapper;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<UserRule> GetUserRules()
        {
            try
            {
                var userRuls = Context.UserRule.ToList();

                return userRuls;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<User> GetUsers()
        {
            try
            {
                var users = Context.User.ToList();

                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<UserTask> GetUserTask()
        {
            try
            {
                var userTasks = Context.UserTask.ToList();

                return userTasks;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserTask GetUserTaskById(int taskId)
        {
            try
            {
                var userTask = Context.UserTask.Where(x => x.UserTaskId == taskId).FirstOrDefault();

                return userTask;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object LeftNavigation()
        {
            //var q = from u in Context.User
            //        where u.IsActive == true
            //        select new
            //        {
            //            ID = u.UserId,
            //            Title = "Dashboards",
            //            Translate = u.UserName,
            //            Type = "collapsable",
            //            Icon = "apps",
            //            Children = from subitem in Context.UserRule
            //                       select new
            //                       {
            //                           ID = subitem.UserRuleId,
            //                           Title = subitem.UserRuleName,
            //                           Type = "item",
            //                           Url = "/user-rule-mapper-list"
            //                       }
            //        };

            //var q = from u in Context.User
            //        where u.IsActive == true
            //        select new
            //        {
            //            ID = u.UserId,
            //            Title = "Dashboards",
            //            Translate = u.UserName,
            //            Type = "collapsable",
            //            Icon = "apps",
            //            Children = from subitem in Context.Vetting
            //                       select subitem
            //        };
            //return q;

            //var q = from a in Context.AdviceCenter
            //        select new
            //        {
            //            ID = a.AdviceCenterId,
            //            Title = "Dashboards",
            //            Type = "collapsable",
            //            Icon = "apps",
            //            Children = from b in Context.AdviceCenterPostComment
            //                                      //AdviceCenterPostComment = from subitem in Context.AdviceCenterPostComment
            //                                      //                          select subitem
            //        };


            //Children = from subcat in Context.ItemCategory
            //           where subcat.ParentId == cat.ItemCategoryId
            //    



            var q2 = from cat in Context.ItemCategory
                     where cat.ParentId == 0
                     select new
                     {
                         ItemCategoryId = cat.ItemCategoryId,
                         ItemCategoryName = cat.ItemCategoryName,
                         Children = from subcat in Context.ItemCategory
                                    where subcat.ParentId == cat.ItemCategoryId
                                    select new
                                    {
                                        subcat.ItemCategoryId
                                    }

                     };
            return q2;


        }
    }
}
