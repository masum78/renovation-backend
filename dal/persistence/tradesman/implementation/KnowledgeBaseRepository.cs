﻿using dal.models;
using dal.repositories.drawhome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dal.persistence.tradesman.implementation
{
    internal class KnowledgeBaseRepository : Repository<HouseType>, IKnowledgeBaseRepository
    {
        private renovationContext _iContext;
        public KnowledgeBaseRepository(renovationContext context) : base(context)
        {
            _iContext = context;
        }

        public renovationContext Context
        {
            get
            {
                return _context as renovationContext;
            }
        }


        public IEnumerable<KnowledgeBase> GetKnowledgeBaseData()
        {
            try
            {
                var knowledgeBase = Context.KnowledgeBase.ToList();

                return knowledgeBase;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
