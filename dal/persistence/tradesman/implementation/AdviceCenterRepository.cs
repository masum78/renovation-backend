﻿using dal.models;
using dal.repositories.tradesman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace dal.persistence.tradesman.implementation
{
    internal class AdviceCenterRepository : Repository<AdviceCenter>, IAdviceCenterRepository
    {
        private renovationContext _iContext;
        public AdviceCenterRepository(renovationContext context) : base(context)
        {
            _iContext = context;
        }
        public renovationContext Context
        {
            get
            {
                return _context as renovationContext;
            }
        }
        public AdviceCenter AdviceCenterPosts()
        {
            try
            {
                var logs = Context.AdviceCenter.OrderByDescending(x => x.AdviceCenterId)
                    .FirstOrDefault(); ;
                return logs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<AdviceCenter> AdviceCenterTitles()
        {
            try
            {
                var adviceCentersTitleList = (from adviceCenter in Context.AdviceCenter
                               select new AdviceCenter
                               {
                                   AdviceCenterId = adviceCenter.AdviceCenterId,
                                   Title = adviceCenter.Title
                               }).ToList();
                return  adviceCentersTitleList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AdviceCenter GetAdviceCenterPostByAdvicecenterId(int advicecenterId)
        {
            try
            {
               // List<AdviceCenterPostComment> adviceCenterPostComments = new List<AdviceCenterPostComment>();

                AdviceCenter adviceCenter = (from adviceCenterObj in Context.AdviceCenter
                                              .Where(x => x.AdviceCenterId == advicecenterId)
                                             select adviceCenterObj
                                              ).FirstOrDefault();


                //IEnumerable<AdviceCenterPostComment> adviceCenterPostComment =
                //    Context.AdviceCenterPostComment.Where(x => x.AdviceCenterId == advicecenterId).ToList();


                var adviceCenterPostComment = (from c in Context.AdviceCenterPostComment
                                               join u in Context.User on c.UserId equals u.UserId
                                               select new AdviceCenterPostComment
                                               {
                                                   AdviceCenterPostCommentId = c.AdviceCenterPostCommentId,
                                                   AdviceCenterId = c.AdviceCenterId,
                                                   Comment = c.Comment,
                                                   IsActive = c.IsActive,
                                                   User = new User
                                                   {
                                                       UserId = c.UserId,
                                                       UserName = u.UserName
                                                   }
                                               }).Where(x => x.AdviceCenterId == advicecenterId && x.IsActive == true).ToList();

                adviceCenter.AdviceCenterPostComment = adviceCenterPostComment.ToList();

                return adviceCenter;
                // .Where(x => x.AdviceCenterId == advicecenterId && x.IsActive == true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<AdviceCenterPostComment> GetCommentByAdviceCenterId(int advicecenterId)
        {
            try
            {
                IEnumerable<AdviceCenterPostComment> adviceCenterPostComment = (from adviceCenterObj in Context.AdviceCenterPostComment
                                              .Where(x => x.AdviceCenterId == advicecenterId)
                                             select adviceCenterObj
                                              ).ToList();


                return adviceCenterPostComment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
