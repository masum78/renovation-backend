﻿

using dal.models;
using dal.repositories.tradesman;
using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace dal.persistence.tradesman.implementation
{
    internal class LoginRepository : Repository<User>, ILoginRepository
    {
        private renovationContext _iContext;
        public LoginRepository(renovationContext context) : base(context)
        {
            _iContext = context;
        }

        public renovationContext Context
        {
            get
            {
                return _context as renovationContext;
            }
        }
        public User GetUserByEmail(User user)
        {
            try
            {
                //var logs = await Context.Tradesman.Where(x => x.PostCode == postcode).ToListAsync();

                var logs = Context.User.Where(x => x.Email == user.Email && x.Password == user.Password).FirstOrDefault();
                return logs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task InactiveUser(User user)
        {
            throw new System.NotImplementedException();
        }

        public async Task<string> InsertItem(User user)
        {
            try
            {

                Context.Set<User>().Add(user);
                await Context.SaveChangesAsync();
                return user.UserName;
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex);
                throw ex;
            }
        }

        public async Task<string> InsertItemTradesman(User user, UserProfile userProfile)
        {
            try
            {
                

                Context.Set<User>().Add(user);
                await Context.SaveChangesAsync();

                Context.Set<UserProfile>().Add(userProfile);
                await Context.SaveChangesAsync();

                return user.UserName;
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex);
                throw ex;
            }
        }

        public Task<bool> IsUserExists(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}
