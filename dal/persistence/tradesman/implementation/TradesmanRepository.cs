﻿using dal.dmo;
using dal.models;
using dal.repositories.tradesman;
using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dal.persistence.tradesman.implementation
{
    internal class TradesmanRepository : Repository<Tradesman>, ITradesmanRepository
    {
        private renovationContext _iContext;
        public TradesmanRepository(renovationContext context) : base(context)
        {
            _iContext = context;
        }

        public renovationContext Context
        {
            get
            {
                return _context as renovationContext;
            }
        }

        public async Task<IEnumerable<Tradesman>> GetItemsUsingPostcodeAsync(string postcode)
        {
            try
            {
                var logs = await Context.Tradesman.Where(x => x.PostCode == postcode).ToListAsync();
                return logs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public  IEnumerable<Tradesman> GetItemsUsingTradesmenName(string tradesmenName)
        {
            try
            {
                var logs = Context.Tradesman.Where(x => x.TradesmanName == tradesmenName);
                return logs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Tradesman> GetItemsUsingCompanyName(string companyName)
        {
            try
            {
                var logs = Context.Tradesman.Where(x => x.CompanyName == companyName);
                return logs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Tradesman> GetTradesmen()
        {
            try
            {
                var a = from t in Context.Tradesman
                        join c in Context.ItemCategory on t.ItemCategoryId equals c.ItemCategoryId
                        join p in Context.UserProfile on t.UserProfileId equals p.UserProfileId

                        select new Tradesman
                        {
                            TradesmanId=t.TradesmanId,
                            TradeLicence = t.TradeLicence,
                            //CompanyLogo = t.CompanyLogo,
                            CompanyName=t.CompanyName,
                            Image=t.Image,
                            //LastUpdate=t.LastUpdate,
                            MobilNo=t.MobilNo,
                            ServiceDescription = t.ServiceDescription,
                            Tags=t.Tags,
                            PostCode=t.PostCode,
                            Active=t.Active,
                            Rate=t.Rate,
                            UserProfile = new UserProfile
                            {
                                UserProfileId =p.UserProfileId,
                                FullName=p.FullName,
                                MobilNo = p.MobilNo
                            },
                            ItemCategory = new ItemCategory
                            {
                                ItemCategoryName =c.ItemCategoryName
                            }

                        };




                //var logs = Context.Tradesman.OrderByDescending(x => x.TradesmanId); ;
                return a;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tradesman GetItemUsingTradesmanId(int tradesmanId)
        {
            try
            {
                var a = from t in Context.Tradesman
                        join c in Context.ItemCategory on t.ItemCategoryId equals c.ItemCategoryId
                        join p in Context.UserProfile on t.UserProfileId equals p.UserProfileId
                        // where t.TradesmanId == tradesmanId
                       // where t.TradesmanId == tradesmanId
                        select new Tradesman
                        {
                            TradesmanId = t.TradesmanId,
                            TradeLicence = t.TradeLicence,
                            //CompanyLogo = t.CompanyLogo,
                            CompanyName = t.CompanyName,
                            Image = t.Image,
                            //LastUpdate = t.LastUpdate,
                            MobilNo = t.MobilNo,
                            ServiceDescription = t.ServiceDescription,
                            Tags = t.Tags,
                            PostCode = t.PostCode,
                            Active = t.Active,
                            Rate = t.Rate,
                            UserProfile = new UserProfile
                            {
                                FullName = p.FullName
                            },
                            ItemCategory = new ItemCategory
                            {
                                ItemCategoryName = c.ItemCategoryName
                            }

                        };
                return a.Where(a=>a.TradesmanId == tradesmanId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> InsertItem(Tradesman tradesman)
        {
            try
            {
                // tradesman.UserProfile.UserProfileId = 3;
                Context.Set<Tradesman>().Add(tradesman);
                await Context.SaveChangesAsync();
                return tradesman.TradesmanName;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<Object> GetItemCategorys()
        {
            try
            {
                List<ItemCategory> sss = (from p in Context.ItemCategory
                                join c in Context.ItemCategory on p.ItemCategoryId equals c.ParentId
                                select new ItemCategory
                                {
                                    ItemCategoryId = p.ItemCategoryId,
                                    ItemCategoryName = p.ItemCategoryName,
                                    ParentId = p.ParentId,
                                    Children = p.Children
                                }).Distinct().ToList();

                


                List<ItemCategory> sss2=null;
                foreach (var item in sss)
                {
                    sss2 = (from p2 in Context.ItemCategory.Where(x => x.ParentId == item.ItemCategoryId)
                            select p2).ToList();
                    foreach (var item2 in sss2)
                    {
                        item.Children.Add(item2);
                    }
                    
                }

                return sss;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<ItemCategory> GetSubCategorys()
        {
            try
            {
                var logs = Context.ItemCategory.Where(x => x.ParentId != 0);
                return logs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ItemCategory GetSubCategoryById(int id)
        {
            try
            {
                ItemCategory itemCategory = Context.ItemCategory.Where(x => x.ItemCategoryId == id).FirstOrDefault();
                return itemCategory;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<ItemCategory> GetAllCategory()
        {
            try
            {
                IEnumerable<ItemCategory> itemCategoryList = Context.ItemCategory.Where(x => x.ParentId == 0);
                return itemCategoryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<ItemCategory> GetSearchItemCategorys()
        {
            try
            {
                // var logs = Context.Tradesman.Where(x => x.CompanyName == companyName);
                var itemCategorys = Context.ItemCategory.ToList();

                return itemCategorys;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void StorePostcods(List<Object> ukPostcods)
        {
            try
            {
                using (var ctx = new renovationContext())
                {
                    ctx.BulkInsert(ukPostcods);
                }
                //using (var transaction = Context.Database.BeginTransaction())
                //{
                //    var bulkConfig = new BulkConfig { PreserveInsertOrder = true, SetOutputIdentity = true };
                //    Context.BulkInsert(ukPostcods, bulkConfig);
                    
                //    transaction.Commit();
                //}


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Tradesman> GetSearchItemCategorys(int selectedItemCategoryId, string postcode)
        {
            try
            {
                // var logs = Context.Tradesman.Where(x => x.CompanyName == companyName);
                var itemCategorys = Context.Tradesman
                    .Where(x => x.ItemCategoryId == selectedItemCategoryId).ToList();
                                    
                return itemCategorys;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertItemSubCategory(ItemCategory itemCategory)
        {
            try
            {
                Context.Set<ItemCategory>().Add(itemCategory);
                Context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<CompanyProfile> GetCompanyProfile(int userId)
        {
            try
            {
                var companyProfile = Context.CompanyProfile
                    .Where(x => x.UserId == userId).ToList();

                return companyProfile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CompanyProfile GetCompanyProfileById(int companyProfileId, int userId)
        {
            try
            {
                var companyProfile = Context.CompanyProfile
                    .Where(x => x.CompanyProfileId ==companyProfileId && x.UserId == userId).FirstOrDefault();

                return companyProfile;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> SaveTradeService(TradeService tradeService)
        {
            try
            {
                Context.Set<TradeService>().Add(tradeService);
                await Context.SaveChangesAsync();
                return tradeService.ServiceDescription;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<TradeService> GetTradeService()
        {
            try
            {
                var a = from t in Context.TradeService
                        join c in Context.ItemCategory on t.ItemCategoryId equals c.ItemCategoryId
                        join p in Context.CompanyProfile on t.CompanyProfileId equals p.CompanyProfileId
                        join u in Context.User on p.UserId equals u.UserId

                        select new TradeService
                        {
                            TradeServiceId = t.TradeServiceId,
                            ServiceDescription = t.ServiceDescription,
                            CellNo = t.CellNo,
                            CompanyProfile = new CompanyProfile
                            {
                                CompanyProfileId = p.CompanyProfileId,
                                CompanyProfileName = p.CompanyProfileName,
                                User = new User
                                {
                                    UserId = u.UserId,
                                    UserName = u.UserName
                                }
                                
                            },
                            ItemCategory = new ItemCategory
                            {
                                ItemCategoryId = c.ItemCategoryId,
                                ItemCategoryName = c.ItemCategoryName
                            },
                            IsChecked = t.IsChecked,
                            IsApproved = t.IsApproved
                        };




                //var logs = Context.Tradesman.OrderByDescending(x => x.TradesmanId); ;
                return a;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Vetting> GetVettingStatus(int tradeServiceId)
        {
            try
            {

                var vettings = from v in Context.Vetting
                               join t in Context.TradeService on v.TradeServiceId equals t.TradeServiceId
                               join c in Context.CompanyProfile on t.CompanyProfileId equals c.CompanyProfileId
                               where t.TradeServiceId == tradeServiceId
                               select new Vetting
                               {
                                   VettingId = v.VettingId,
                                   TradeServiceId = t.TradeServiceId,
                                   VettingHead = v.VettingHead,
                                   Status = v.Status,
                                   VettingValueofHead = v.VettingValueofHead,
                                   Comments = v.Comments,
                                   TradeService = new TradeService
                                   {
                                       CompanyProfile = new CompanyProfile
                                       {
                                           CompanyProfileName = c.CompanyProfileName
                                       }
                                   }
                               };

                return vettings;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Menu1> GetNavigation()
        {
            try
            {
                List<Menu1> menus = new List<Menu1>();
                return menus;

                //var menu = from m in Context.Menu
                //               join s in Context.SubMenu on m.Children equals s.SubMenuId
                //               select new Menu
                //               {
                //                   MenuId = m.MenuId,
                //                   Title = m.Title,
                //                   Translate =m.Translate,
                //                   Type =m.Type,
                //                   Icon = m.Icon,
                //                   Children = new SubMenu{

                //                   }

                //                   //TradeService = new TradeService
                //                   //{
                //                   //    CompanyProfile = new CompanyProfile
                //                   //    {
                //                   //        CompanyProfileName = c.CompanyProfileName
                //                   //    }
                //                   //}
                //               };

                //return menu;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
