﻿using dal.models;
using dal.repositories.drawhome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dal.persistence.tradesman.implementation
{
    internal class DrawHomeRepository : Repository<HouseType>, IDrawHomeRepository
    {
        private renovationContext _iContext;
        public DrawHomeRepository(renovationContext context) : base(context)
        {
            _iContext = context;
        }

        public renovationContext Context
        {
            get
            {
                return _context as renovationContext;
            }
        }

        public IEnumerable<HouseType> GetHouseTypes()
        {
            try
            {
                var houseType = Context.HouseType.ToList();

                return houseType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<HouseTypeLayout> GetLayouts()
        {
            try
            {
                var houseTypeLauout = Context.HouseTypeLayout.ToList();

                return houseTypeLauout;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CostSetup> GetCostSetupData()
        {
            try
            {
                var costSetups = from c in Context.CostSetup
                        join m in Context.Materials on c.MaterialId equals m.MaterialId
                        
                        select new CostSetup
                        {
                            CostSetupId=c.CostSetupId,
                            MaterialId = m.MaterialId,
                            Material = new Material
                            {
                                MaterialId = m.MaterialId,
                                MaterialName = m.MaterialName
                            },
                            Quantity = c.Quantity,
                            Cost = c.Cost
                        };
                return costSetups;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Product> GetProducts(int materialId)
        {
            try
            {
                var products = Context.Products.Where(x=>x.MaterialId == materialId).ToList();

                return products;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Material> GetMaterials()
        {
            try
            {
                var products = Context.Materials.ToList();

                return products;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Material> GetMaterialById(int materialId)
        {
            try
            {
                var products = Context.Materials.Where(x=>x.MaterialId == materialId).ToList();

                return products;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<HouseTypeLayout> StoreLayout(HouseTypeLayout houseTypeLayout, int userId)
        {
            try
            {
                houseTypeLayout.LayoutName = "Layout" + Context.HouseType.ToList().Count;
                houseTypeLayout.UserId = userId;
                houseTypeLayout.HouseTypeId = 1;
                Context.Set<HouseTypeLayout>().Add(houseTypeLayout);
                Context.SaveChanges();
                return Context.HouseTypeLayout.Where(userid => userid.UserId == userId).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        
    }
}
