﻿using dal.models;
using dal.persistence.tradesman.implementation;
using dal.repositories.drawhome;
using dal.repositories.tradesman;
using dal.repositories.user;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace dal.persistence.tradesman
{
    class UnitOfWork : IUnitOfWork
    {
        private readonly renovationContext _context;

        public ITradesmanRepository Tradesman { get; private set; }
        public IAdviceCenterRepository AdviceCenter { get; set; }
        public ILoginRepository Login { get; set; }

        public IDrawHomeRepository DrawHome { get; set; }

        public IKnowledgeBaseRepository KnowledgeBaseRepository { get; set; }
        public IUserRepository User { get; set; }

        //public IAdviceCenterRepository AdviceCenter { get; private set; }

        // public IAdviceCenterRepository AdviceCenter { get; private set; }

        public UnitOfWork(renovationContext context)
        {
            _context = context;
            Tradesman = new TradesmanRepository(_context);
            AdviceCenter = new AdviceCenterRepository(_context);
            Login = new LoginRepository(_context);
            DrawHome = new DrawHomeRepository(_context);
            KnowledgeBaseRepository = new KnowledgeBaseRepository(_context);
            User = new UserRepository(_context);

        }
        public int Complete()
        {
            return _context.SaveChanges();
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
