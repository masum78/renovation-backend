﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace dal.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HouseType",
                columns: table => new
                {
                    HouseTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HouseTypeName = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseType", x => x.HouseTypeId);
                });

            migrationBuilder.CreateTable(
                name: "ItemCategory",
                columns: table => new
                {
                    ItemCategoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ItemCategoryName = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: false),
                    ItemCategoryId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemCategory", x => x.ItemCategoryId);
                    table.ForeignKey(
                        name: "FK_ItemCategory_ItemCategory_ItemCategoryId1",
                        column: x => x.ItemCategoryId1,
                        principalTable: "ItemCategory",
                        principalColumn: "ItemCategoryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "KnowledgeBase",
                columns: table => new
                {
                    KnowledgeBaseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WarningCode = table.Column<string>(nullable: true),
                    WarningText = table.Column<string>(nullable: true),
                    WarningUrl = table.Column<string>(nullable: true),
                    PermissionRequirements = table.Column<string>(nullable: true),
                    RefusedApplication = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KnowledgeBase", x => x.KnowledgeBaseId);
                });

            migrationBuilder.CreateTable(
                name: "Material",
                columns: table => new
                {
                    MaterialId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaterialName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Material", x => x.MaterialId);
                });

            migrationBuilder.CreateTable(
                name: "SingleInt",
                columns: table => new
                {
                    RetrievedData = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SingleInt", x => x.RetrievedData);
                });

            migrationBuilder.CreateTable(
                name: "SingleString",
                columns: table => new
                {
                    RetrievedData = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SingleString", x => x.RetrievedData);
                });

            migrationBuilder.CreateTable(
                name: "UkPostcodes",
                columns: table => new
                {
                    PostcodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    pcd = table.Column<string>(nullable: true),
                    pcd2 = table.Column<string>(nullable: true),
                    pcds = table.Column<string>(nullable: true),
                    lat = table.Column<string>(nullable: true),
                    lon = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UkPostcodes", x => x.PostcodeId);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Tradesman = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "CostSetup",
                columns: table => new
                {
                    CostSetupId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaterialId = table.Column<int>(nullable: false),
                    Quantity = table.Column<string>(nullable: true),
                    Cost = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CostSetup", x => x.CostSetupId);
                    table.ForeignKey(
                        name: "FK_CostSetup_Material_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Material",
                        principalColumn: "MaterialId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductDescription = table.Column<string>(nullable: true),
                    MaterialId = table.Column<int>(nullable: false),
                    ProductName = table.Column<string>(nullable: true),
                    ProductUrl = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_Products_Material_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Material",
                        principalColumn: "MaterialId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyProfile",
                columns: table => new
                {
                    CompanyProfileId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyProfileName = table.Column<string>(nullable: true),
                    CompanyDescription = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    Accreditations = table.Column<string>(nullable: true),
                    Insurance = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    CompanyAddress = table.Column<string>(nullable: true),
                    Identification = table.Column<string>(nullable: true),
                    LogoPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyProfile", x => x.CompanyProfileId);
                    table.ForeignKey(
                        name: "FK_CompanyProfile_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HouseTypeLayout",
                columns: table => new
                {
                    HouseTypeLayoutId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LayoutName = table.Column<string>(nullable: true),
                    HouseTypeId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Width = table.Column<float>(nullable: false),
                    Length = table.Column<float>(nullable: false),
                    FloorThickness = table.Column<float>(nullable: false),
                    WallThickness = table.Column<float>(nullable: false),
                    WallHeight = table.Column<float>(nullable: false),
                    NBFloors = table.Column<float>(nullable: false),
                    RoofHeight = table.Column<float>(nullable: false),
                    RoofThickness = table.Column<float>(nullable: false),
                    RoofTopLength = table.Column<float>(nullable: false),
                    RoofBottomLength = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseTypeLayout", x => x.HouseTypeLayoutId);
                    table.ForeignKey(
                        name: "FK_HouseTypeLayout_HouseType_HouseTypeId",
                        column: x => x.HouseTypeId,
                        principalTable: "HouseType",
                        principalColumn: "HouseTypeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HouseTypeLayout_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserProfile",
                columns: table => new
                {
                    UserProfileId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    ProfilePicture = table.Column<string>(nullable: true),
                    TradeLicence = table.Column<string>(nullable: true),
                    MobilNo = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfile", x => x.UserProfileId);
                    table.ForeignKey(
                        name: "FK_UserProfile_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TradeService",
                columns: table => new
                {
                    TradeServiceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyProfileId = table.Column<int>(nullable: false),
                    ItemCategoryId = table.Column<int>(nullable: false),
                    ServiceDescription = table.Column<string>(nullable: true),
                    CellNo = table.Column<string>(nullable: true),
                    IsChecked = table.Column<bool>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeService", x => x.TradeServiceId);
                    table.ForeignKey(
                        name: "FK_TradeService_CompanyProfile_CompanyProfileId",
                        column: x => x.CompanyProfileId,
                        principalTable: "CompanyProfile",
                        principalColumn: "CompanyProfileId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TradeService_ItemCategory_ItemCategoryId",
                        column: x => x.ItemCategoryId,
                        principalTable: "ItemCategory",
                        principalColumn: "ItemCategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HouseBuildingCost",
                columns: table => new
                {
                    HouseBuildingCostId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    HouseTypeLayoutId = table.Column<int>(nullable: false),
                    MaterialName = table.Column<string>(nullable: true),
                    Quantity = table.Column<float>(nullable: false),
                    Unit = table.Column<string>(nullable: true),
                    ApproximateCost = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseBuildingCost", x => x.HouseBuildingCostId);
                    table.ForeignKey(
                        name: "FK_HouseBuildingCost_HouseTypeLayout_HouseTypeLayoutId",
                        column: x => x.HouseTypeLayoutId,
                        principalTable: "HouseTypeLayout",
                        principalColumn: "HouseTypeLayoutId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdviceCenter",
                columns: table => new
                {
                    AdviceCenterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    PostText = table.Column<string>(nullable: true),
                    AuthorName = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    PostImage = table.Column<string>(nullable: true),
                    UserProfileId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdviceCenter", x => x.AdviceCenterId);
                    table.ForeignKey(
                        name: "FK_AdviceCenter_UserProfile_UserProfileId",
                        column: x => x.UserProfileId,
                        principalTable: "UserProfile",
                        principalColumn: "UserProfileId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tradesman",
                columns: table => new
                {
                    TradesmanId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TradesmanName = table.Column<string>(nullable: true),
                    UserProfileId = table.Column<int>(nullable: false),
                    ItemCategoryId = table.Column<int>(nullable: false),
                    TradeLicence = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true),
                    ServiceDescription = table.Column<string>(nullable: true),
                    MobilNo = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    Rate = table.Column<double>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tradesman", x => x.TradesmanId);
                    table.ForeignKey(
                        name: "FK_Tradesman_ItemCategory_ItemCategoryId",
                        column: x => x.ItemCategoryId,
                        principalTable: "ItemCategory",
                        principalColumn: "ItemCategoryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tradesman_UserProfile_UserProfileId",
                        column: x => x.UserProfileId,
                        principalTable: "UserProfile",
                        principalColumn: "UserProfileId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vetting",
                columns: table => new
                {
                    VettingId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TradeServiceId = table.Column<int>(nullable: false),
                    VettingHead = table.Column<string>(nullable: true),
                    VettingValueofHead = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Comments = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vetting", x => x.VettingId);
                    table.ForeignKey(
                        name: "FK_Vetting_TradeService_TradeServiceId",
                        column: x => x.TradeServiceId,
                        principalTable: "TradeService",
                        principalColumn: "TradeServiceId",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.CreateTable(
            //    name: "AdviceCenterPostComment",
            //    columns: table => new
            //    {
            //        AdviceCenterPostCommentId = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        AdviceCenterId = table.Column<int>(nullable: false),
            //        Comment = table.Column<string>(nullable: true),
            //        UserId = table.Column<int>(nullable: false),
            //        CreateDate = table.Column<DateTime>(nullable: false),
            //        IsActive = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AdviceCenterPostComment", x => x.AdviceCenterPostCommentId);
            //        table.ForeignKey(
            //            name: "FK_AdviceCenterPostComment_AdviceCenter_AdviceCenterId",
            //            column: x => x.AdviceCenterId,
            //            principalTable: "AdviceCenter",
            //            principalColumn: "AdviceCenterId",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_AdviceCenterPostComment_User_UserId",
            //            column: x => x.UserId,
            //            principalTable: "User",
            //            principalColumn: "UserId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "UserFeedback",
            //    columns: table => new
            //    {
            //        UserFeedbackId = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        UserId = table.Column<int>(nullable: false),
            //        TradesmanId = table.Column<int>(nullable: false),
            //        Comments = table.Column<string>(nullable: true),
            //        RatingPoint = table.Column<int>(nullable: false),
            //        CreateDate = table.Column<DateTime>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_UserFeedback", x => x.UserFeedbackId);
            //        table.ForeignKey(
            //            name: "FK_UserFeedback_Tradesman_TradesmanId",
            //            column: x => x.TradesmanId,
            //            principalTable: "Tradesman",
            //            principalColumn: "TradesmanId",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_UserFeedback_User_UserId",
            //            column: x => x.UserId,
            //            principalTable: "User",
            //            principalColumn: "UserId",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            migrationBuilder.CreateIndex(
                name: "IX_AdviceCenter_UserProfileId",
                table: "AdviceCenter",
                column: "UserProfileId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AdviceCenterPostComment_AdviceCenterId",
            //    table: "AdviceCenterPostComment",
            //    column: "AdviceCenterId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AdviceCenterPostComment_UserId",
            //    table: "AdviceCenterPostComment",
            //    column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyProfile_UserId",
                table: "CompanyProfile",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_CostSetup_MaterialId",
                table: "CostSetup",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_HouseBuildingCost_HouseTypeLayoutId",
                table: "HouseBuildingCost",
                column: "HouseTypeLayoutId");

            migrationBuilder.CreateIndex(
                name: "IX_HouseTypeLayout_HouseTypeId",
                table: "HouseTypeLayout",
                column: "HouseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_HouseTypeLayout_UserId",
                table: "HouseTypeLayout",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemCategory_ItemCategoryId1",
                table: "ItemCategory",
                column: "ItemCategoryId1");

            migrationBuilder.CreateIndex(
                name: "IX_Products_MaterialId",
                table: "Products",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_TradeService_CompanyProfileId",
                table: "TradeService",
                column: "CompanyProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_TradeService_ItemCategoryId",
                table: "TradeService",
                column: "ItemCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Tradesman_ItemCategoryId",
                table: "Tradesman",
                column: "ItemCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Tradesman_UserProfileId",
                table: "Tradesman",
                column: "UserProfileId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_UserFeedback_TradesmanId",
            //    table: "UserFeedback",
            //    column: "TradesmanId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_UserFeedback_UserId",
            //    table: "UserFeedback",
            //    column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfile_UserId",
                table: "UserProfile",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Vetting_TradeServiceId",
                table: "Vetting",
                column: "TradeServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdviceCenterPostComment");

            migrationBuilder.DropTable(
                name: "CostSetup");

            migrationBuilder.DropTable(
                name: "HouseBuildingCost");

            migrationBuilder.DropTable(
                name: "KnowledgeBase");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "SingleInt");

            migrationBuilder.DropTable(
                name: "SingleString");

            migrationBuilder.DropTable(
                name: "UkPostcodes");

            migrationBuilder.DropTable(
                name: "UserFeedback");

            migrationBuilder.DropTable(
                name: "Vetting");

            migrationBuilder.DropTable(
                name: "AdviceCenter");

            migrationBuilder.DropTable(
                name: "HouseTypeLayout");

            migrationBuilder.DropTable(
                name: "Material");

            migrationBuilder.DropTable(
                name: "Tradesman");

            migrationBuilder.DropTable(
                name: "TradeService");

            migrationBuilder.DropTable(
                name: "HouseType");

            migrationBuilder.DropTable(
                name: "UserProfile");

            migrationBuilder.DropTable(
                name: "CompanyProfile");

            migrationBuilder.DropTable(
                name: "ItemCategory");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
