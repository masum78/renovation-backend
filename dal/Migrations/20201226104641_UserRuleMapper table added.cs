﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace dal.Migrations
{
    public partial class UserRuleMappertableadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserRuleMapper",
                columns: table => new
                {
                    UserRuleMapperId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    UserRuleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRuleMapper", x => x.UserRuleMapperId);
                    table.ForeignKey(
                        name: "FK_UserRuleMapper_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRuleMapper_UserRule_UserRuleId",
                        column: x => x.UserRuleId,
                        principalTable: "UserRule",
                        principalColumn: "UserRuleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserRuleMapper_UserId",
                table: "UserRuleMapper",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRuleMapper_UserRuleId",
                table: "UserRuleMapper",
                column: "UserRuleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserRuleMapper");
        }
    }
}
