﻿
using System.ComponentModel.DataAnnotations;

namespace dal.dmo
{
    class SingleString
    {
        [Key]
        public string RetrievedData { get; set; }
    }
}
