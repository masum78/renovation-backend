﻿using dal.repositories.drawhome;
using dal.repositories.tradesman;
using dal.repositories.user;
using System;
using System.Threading.Tasks;

namespace dal
{
   public interface IUnitOfWork : IDisposable
    {
        ITradesmanRepository Tradesman { get; }
        IAdviceCenterRepository AdviceCenter { get; set; }
        ILoginRepository Login { get; set; }
        IDrawHomeRepository DrawHome { get; set; }
        IKnowledgeBaseRepository KnowledgeBaseRepository { get; set; }
        IUserRepository User { get; set; }
        int Complete();

        Task<int> CompleteAsync();
    }
}
