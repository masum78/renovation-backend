﻿using System;
using System.Collections.Generic;
using System.Text;
using dal.models;

namespace dal.repositories.user
{
   public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetUsers();
        User GetUserById(int userId);
        IEnumerable<UserRule> GetUserRules();
        UserRule GetUserRuleById(int ruleId);
        IEnumerable<UserTask> GetUserTask();
        UserTask GetUserTaskById(int taskId);
        IEnumerable<UserRuleMapper> GetUserRuleMapper();
        Object LeftNavigation();
        
    }
}
