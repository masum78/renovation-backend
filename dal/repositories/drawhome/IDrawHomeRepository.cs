﻿using dal.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace dal.repositories.drawhome
{
   public interface IDrawHomeRepository
    {
        IEnumerable<HouseType> GetHouseTypes();
        IEnumerable<HouseTypeLayout> StoreLayout(HouseTypeLayout houseTypeLayout, int userId);
        IEnumerable<HouseTypeLayout> GetLayouts();
        IEnumerable<Product> GetProducts(int materialId);
        IEnumerable<Material> GetMaterials();
        IEnumerable<Material> GetMaterialById(int materialId);
        IEnumerable<CostSetup> GetCostSetupData();
        
    }
}
