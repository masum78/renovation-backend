﻿using dal.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace dal.repositories.drawhome
{
   public interface IKnowledgeBaseRepository
    {
        IEnumerable<KnowledgeBase> GetKnowledgeBaseData();
    }
}
