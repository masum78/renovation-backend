﻿using dal.dmo;
using dal.models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace dal.repositories.tradesman
{
  public  interface ITradesmanRepository : IRepository<Tradesman>
    {
        IEnumerable<Tradesman> GetTradesmen();
        IEnumerable<ItemCategory> GetSearchItemCategorys();
        IEnumerable<Object> GetItemCategorys();
        IEnumerable<Menu1> GetNavigation();

        IEnumerable<ItemCategory> GetSubCategorys();
        ItemCategory GetSubCategoryById(int id);
        IEnumerable<ItemCategory> GetAllCategory();
        void InsertItemSubCategory(ItemCategory itemCategory);
        IEnumerable<Tradesman> GetSearchItemCategorys(int selectedItemCategoryId, string postcode);
        Task<IEnumerable<Tradesman>> GetItemsUsingPostcodeAsync(string postcode);

        IEnumerable<Tradesman> GetItemsUsingTradesmenName(string tradesmenName);

        IEnumerable<Tradesman> GetItemsUsingCompanyName(string companyName);

        Tradesman GetItemUsingTradesmanId(int tradesmanId);

        void StorePostcods(List<Object> ukPostcods);
        Task<string> InsertItem(Tradesman tradesman);
        IEnumerable<TradeService> GetTradeService();
       
        IEnumerable<Vetting> GetVettingStatus(int tradeServiceId);
        Task<string> SaveTradeService(TradeService tradeService);
        IEnumerable<CompanyProfile> GetCompanyProfile(int userId);
        CompanyProfile GetCompanyProfileById(int companyProfileId, int userId);
    }

}
