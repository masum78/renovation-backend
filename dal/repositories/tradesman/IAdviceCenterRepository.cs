﻿using dal.models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace dal.repositories.tradesman
{
   public interface IAdviceCenterRepository : IRepository<AdviceCenter>
    {
        AdviceCenter AdviceCenterPosts();
        IEnumerable<AdviceCenter> AdviceCenterTitles();
        AdviceCenter GetAdviceCenterPostByAdvicecenterId(int advicecenterId);
        IEnumerable<AdviceCenterPostComment> GetCommentByAdviceCenterId(int advicecenterId);
    }
}
