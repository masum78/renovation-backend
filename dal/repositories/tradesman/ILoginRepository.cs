﻿using dal.models;
using System.Threading.Tasks;

namespace dal.repositories.tradesman
{
   public interface ILoginRepository
    {
        Task<string> InsertItem(User user);
        Task<string> InsertItemTradesman(User user, UserProfile userProfile);
        
        Task<bool> IsUserExists(User user);
        Task InactiveUser(User user);
        User GetUserByEmail(User user);
    }
}
