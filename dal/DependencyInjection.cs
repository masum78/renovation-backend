﻿using dal.models;
using dal.persistence.tradesman;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace dal
{
  public static class DependencyInjection
    {
        public static IServiceCollection AddDataAccess(this IServiceCollection services, string connection)
        {
            services.AddDbContext<renovationContext>(options => options.UseSqlServer(connection));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
