﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using bll.dto;
using bll.manager.tradesman;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace advicecenter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdviceCenterController : ControllerBase
    {
        private readonly IAdviceCenterManager _adviceCenterManager;
        public AdviceCenterController(IAdviceCenterManager adviceCenterManager)
        {
            _adviceCenterManager = adviceCenterManager;
        }

        [HttpGet]
        [Route("GetAdviceCenterPosts")]
        public ActionResult GetAdviceCenterPosts()
        {
            try
            {
                var data =  _adviceCenterManager.GetAdviceCenterPosts();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetAdviceCenterTitleList")]
        public ActionResult GetAdviceCenterTitleList()
        {
            try
            {
                var data = _adviceCenterManager.AdviceCenterTitleList();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("GetAdviceCenterPostByAdvicecenterId/{adviceCenterId}")]
        public ActionResult GetAdviceCenterPostByAdvicecenterId(int adviceCenterId)
        {
            try
            {
                var data = _adviceCenterManager.GetAdviceCenterPostByAdvicecenterId(adviceCenterId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("InsertItem")]
        public IActionResult InsertItem([FromBody] AdviceCenterPostCommentDto adviceCenterPostCommentDto)
        {
            try
            {
                adviceCenterPostCommentDto.IsActive = true;
                adviceCenterPostCommentDto.CreateDate = DateTime.Now;

                AdviceCenterDto adviceCenterDto = _adviceCenterManager.GetAdviceCenterPosts(); ;
                //string tradesmenId = await _tradesmanManager.InsertAsync(trademanDto);
                return Ok();
            }
            catch (Exception x)
            {
                throw x;
            }
        }


        // GET api/<AdviceCenterController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<AdviceCenterController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<AdviceCenterController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<AdviceCenterController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
