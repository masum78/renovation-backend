﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bll.dto;
using bll.manager.tradesman;
using getAddress.Sdk;
using getAddress.Sdk.Api;
using getAddress.Sdk.Api.Requests;
//using MarkEmbling.PostcodesIO;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace tradesperson.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TradesmanController : ControllerBase
    {
        private readonly ITradesmanManager _tradesmanManager;

        public TradesmanController(ITradesmanManager purchaseOrderManager)
        {
            _tradesmanManager = purchaseOrderManager;
        }

        [HttpGet]
        [Route("GetTradesmen")]
        public ActionResult GetTradesmen()
        {

            try
            {
                var data = _tradesmanManager.GetTradesmen();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GetItemUsingTradesmanId
        [HttpGet]
        [Route("GetItemUsingTradesmanId/{tradesmanId}")]
        public ActionResult GetItemUsingTradesmanId(int tradesmanId)
        {

            try
            {
                var data = _tradesmanManager.GetItemUsingTradesmanId(tradesmanId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetItemsUsingPostcode/{postcode}")]
        public ActionResult GetItemsUsingPostcode(string postcode)
        {

            try
            {
                var data = _tradesmanManager.GetItemsUsingPostcode(postcode);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetItemsUsingTradesmenName/{tradesmenName}")]
        public ActionResult GetItemsUsingTradesmenName(string tradesmenName)
        {

            try
            {
                var data = _tradesmanManager.GetItemsUsingTradesmenName(tradesmenName);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetItemsUsingCompanyName/{companyName}")]
        public ActionResult GetItemsUsingCompanyName(string companyName)
        {

            try
            {
                var data = _tradesmanManager.GetItemsUsingCompanyName(companyName);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        
        [HttpGet]
        [Route("FindTrademanByPostcode/{postcode}")]
        public async Task<ActionResult> FindTrademanByPostcode(string postcode)
        {
            // GetTrademanByPostcode(string postcode)
            try
            {
                var data = await _tradesmanManager.GetTrademanByPostcode(postcode);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("FindTrademan")]
        public async Task<ActionResult> FindTrademan()
        {

            try
            {
                var data = await _tradesmanManager.GetTrademanListAsync();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetCompanyProfile/{userId}")]
        public ActionResult GetCompanyProfile(int userId)
        {
            try
            {
                var data = _tradesmanManager.GetCompanyProfile(userId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetCompanyProfileById/{companyProfileId}/{userId}")]
        public ActionResult GetCompanyProfileById(int companyProfileId, int userId)
        {
            try
            {
                var data = _tradesmanManager.GetCompanyProfileById(companyProfileId, userId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetTradeService")]
        public ActionResult GetTradeService()
        {

            try
            {
                var data = _tradesmanManager.GetTradeService();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpGet]
        [Route("GetVettingStatus/{tradeServiceId}")]
        public ActionResult GetVettingStatus(int tradeServiceId)
        {

            try
            {
                var data = _tradesmanManager.GetVettingStatus(tradeServiceId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("SaveTradeService")]
        public async Task<IActionResult> SaveTradeService([FromBody] TradeServiceDto tradeServiceDto)
        {
            try
            {
                //trademanDto.UserProfileId = 3;
                string tradesmenId = await _tradesmanManager.SaveTradeService(tradeServiceDto);
                return Ok();
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        [HttpGet]
        [Route("GetTradesmanListUsingGetAddress")]
        public async Task<ActionResult> GetTradesmanListUsingGetAddress()
        {

            try
            {
                var apiKey = new ApiKey("ZqGpiKpzaUKzRFm39VaCxQ26331");

                IAddressService addressService = new AddressService(apiKey);

                var result = await addressService.Get(new GetAddressRequest("ol6 9nr", null));
                List<string> addressList = new List<string>();

                if (result.IsSuccess)
                {
                    

                    var successfulResult = result.SuccessfulResult;

                    var latitude = successfulResult.Latitude;

                    var Longitude = successfulResult.Longitude;

                    foreach (var address in successfulResult.Addresses)
                    {
                        var line1 = address.Line1;
                        var line2 = address.Line2;
                        var line3 = address.Line3;
                        var line4 = address.Line4;
                        var locality = address.Locality;
                        var townOrCity = address.TownOrCity;
                        var county = address.County;
                        string formatedAddress = line1 + " " + townOrCity + " " + county;
                        addressList.Add(formatedAddress);
                    }
                }
                return Ok(addressList);

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        //[HttpGet]
        //[Route("GetTradesmanListUsingPostcodes")]
        //public async Task<ActionResult> GetTradesmanListUsingPostcodes()
        //{

        //    try
        //    {

        //        var client = new PostcodesIOClient();
        //        var result = client.Lookup("GU1 1AA");
        //        var result2 = client.Nearest("m13 0ud", null, null); //Nearest postcodes for postcode
        //        var result3 = client.Lookup("m13 0ud");
        //        var result4 = client.Lookup("ol6 9nr"); //Lookup a postcode
        //                                                      //var result5 = client.BulkLookup("","");
        //        var result5 = client.Lookup("ol6 9nr").Codes;
        //        var data = await _tradesmanManager.GetTrademanListAsync();
        //        if (data == null)
        //        {
        //            return NotFound();
        //        }

        //        return Ok(result4);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex);
        //    }
        //}

        // GET: api/<TradesmanController>
        [HttpGet]
        public TrademanDto Get()
        {
            return new TrademanDto
            {
                TradeLicence = "001AQW",
                TradesmanId = 1,
                TradesmanName = "Imran Khan"
            };
        }

        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        [Route("InsertItem")]
        public async Task<IActionResult> InsertItem([FromBody] TrademanDto trademanDto)
        {
            try
            {
                trademanDto.UserProfileId = 3;
                string tradesmenId = await _tradesmanManager.InsertAsync(trademanDto);
                return Ok();
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        // PUT api/<TradesmanController>/5
        [HttpPut("UpdateTradesmanItem")]
        public async Task<IActionResult> UpdateTradesmanItem([FromBody] TrademanDto trademanDto)
        {
            
            try
            {
                // Checking whether the updatable object is null
                if (trademanDto == null)
                {
                    return NotFound();
                }
                await _tradesmanManager.UpdateData(trademanDto);

                return Ok();
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        // DELETE api/<TradesmanController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
