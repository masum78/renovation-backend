﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using bll.dto;
using bll.manager.tradesman;
using dal.models;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


using IO.ClickSend.ClickSend.Api;
using IO.ClickSend.Client;
using IO.ClickSend.ClickSend.Model;


namespace tradesperson.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ItemcategoryController : ControllerBase
    {
        private readonly ITradesmanManager _tradesmanManager;

        public ItemcategoryController(ITradesmanManager tradesmanManager)
        {
            _tradesmanManager = tradesmanManager;
        }


        [HttpGet]
        [Route("SendSMS")]
        public void SendSMS()
        {
            try
            {
                var configuration = new Configuration()
                {
                    Username = "mmaasum@yahoo.com",
                    Password = "48BB1AC3-2976-28F6-57C1-12104E847D72"
                };
                var smsApi = new SMSApi(configuration);

                var listOfSms = new List<SmsMessage>
                            {
                                new SmsMessage(
                                    to: "+8801914319296",
                                    body: "test message",
                                    source: "sdk"
                                )
                            };

                var smsCollection = new SmsMessageCollection(listOfSms);
                var response = smsApi.SmsSendPost(smsCollection);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        [HttpGet]
        [Route("GetNavigation")]
        public ActionResult GetNavigation()
        {

            try
            {
                var data = _tradesmanManager.GetItemCategorys();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetItemgategorys")]
        public ActionResult GetItemgategorys()
        {

            try
            {
                var data = _tradesmanManager.GetItemCategorys();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetSubCategory")]
        public ActionResult GetSubCategory()
        {

            try
            {
                var data = _tradesmanManager.GetSubCategorys();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetSubCategory/{subCategoryId}")]
        public ActionResult GetSubCategory(int subCategoryId)
        {

            try
            {
                var data = _tradesmanManager.GetSubCategoryById(subCategoryId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetAllCategory")]
        public ActionResult GetAllCategory()
        {
            try
            {
                var data = _tradesmanManager.GetAllCategory();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("InsertItemSubCategory")]
        public async Task<IActionResult> InsertItemSubCategory([FromBody] ItemCategoryDto itemCategoryDto)
        {
            try
            {
                var data = _tradesmanManager.GetAllCategory();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            //try
            //{
            //    string tradesmenId = await _tradesmanManager.InsertAsync(trademanDto);
            //    return Ok();
            //}
            //catch (Exception x)
            //{
            //    throw x;
            //}
        }

        [HttpGet]
        [Route("GetSearchItemCategorys")]
        public ActionResult GetSearchItemCategorys()
        {

            try
            {
                var data = _tradesmanManager.GetSearchItemCategorys();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpGet]
        [Route("GetItemgategorys/{selectedItemCategoryId}/{postcode}")]
        public ActionResult GetSearchItemgategorys(int selectedItemCategoryId, string postcode)
        {

            try
            {
                var data = _tradesmanManager.GetSearchItemCategorys(selectedItemCategoryId, postcode);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("ImportExcel")]
        public ActionResult ImportExcel()
        {
            //List<FilePath> files = new List<FilePath>();
            List<IFormFile> files = new List<IFormFile>();
            DirectoryInfo dirInfo = new DirectoryInfo(@"E:/drive-download-20200902T160343Z-001/");
            List<Object> ukPostcodesList = new List<Object>();
            foreach (FileInfo fInfo in dirInfo.GetFiles())
            {
                string fileName = fInfo.FullName;
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateCsvReader(stream))
                    {
                        while (reader.Read()) //Each row of the file
                        {
                            ukPostcodesList.Add(new UkPostcodes
                            {
                                PostcodeId = 0,
                                pcd = reader.GetValue(0).ToString(),
                                pcd2 = reader.GetValue(1).ToString(),
                                pcds = reader.GetValue(2).ToString(),
                                lat = reader.GetValue(33).ToString(),
                                lon = reader.GetValue(34).ToString(),
                                DateCreated = DateTime.Now
                            });
                        }
                    }
                }
                _tradesmanManager.StorePostcods(ukPostcodesList);
            }

            //return files.ToList();


            // [FromBody] List<IFormFile> files
            //List<IFormFile> files=null;
            //List<Object> ukPostcodesList = new List<Object>();
            ////var fileName = "./Users.xlsx";
            ////var fileName = "E:/drive-download-20200902T160343Z-001/NSPL_NOV_2019_UK_AB.xlsx";
            //string fileName = @"E://drive-download-20200902T160343Z-001//NSPL_NOV_2019_UK_AB.csv";
            //System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            //using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            //{
            //    using (var reader = ExcelReaderFactory.CreateCsvReader(stream))
            //    {
            //        while (reader.Read()) //Each row of the file
            //        {
            //            ukPostcodesList.Add(new UkPostcodes
            //            {
            //                PostcodeId = 0,
            //                pcd = reader.GetValue(0).ToString(),
            //                pcd2 = reader.GetValue(1).ToString(),
            //                pcds = reader.GetValue(2).ToString(),
            //                lat = reader.GetValue(33).ToString(),
            //                lon = reader.GetValue(34).ToString(),
            //                DateCreated = DateTime.Now
            //            });
            //        }
            //    }
            //}

            //long size = files.Sum(f => f.Length);

            //foreach (var formFile in files)
            //{
            //    if (formFile.Length > 0)
            //    {
            //        var filePath = Path.GetTempFileName();
            //        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            //        using (var stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read))
            //        {
            //            using (var reader = ExcelReaderFactory.CreateCsvReader(stream))
            //            {
            //                while (reader.Read()) //Each row of the file
            //                {
            //                    ukPostcodesList.Add(new UkPostcodes
            //                    {
            //                        PostcodeId = 0,
            //                        pcd = reader.GetValue(0).ToString(),
            //                        pcd2 = reader.GetValue(1).ToString(),
            //                        pcds = reader.GetValue(2).ToString(),
            //                        lat = reader.GetValue(33).ToString(),
            //                        lon = reader.GetValue(34).ToString(),
            //                        DateCreated = DateTime.Now
            //                    });
            //                }
            //            }
            //        }
            //    }
            //}

            //foreach (var formFile in files)
            //{
            //    if (formFile.Length > 0)
            //    {
            //        var filePath = Path.GetTempFileName();

            //        using (var stream = System.IO.File.Create(filePath))
            //        {
            //            await formFile.CopyToAsync(stream);
            //        }
            //    }
            //}

            try
            {
               // _tradesmanManager.StorePostcods(ukPostcodesList);

                //if (data == null)
                //{
                //    return NotFound();
                //}
                //return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }


            return Ok();
        }
    }

}
