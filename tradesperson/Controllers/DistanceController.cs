﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace tradesperson.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DistanceController : ControllerBase
    {
        [HttpGet]
        [Route("GetUkDistance")]
        public ActionResult GetUkDistance()
        {
            try
            {
                var distance = Distance.BetweenTwoUKPostCodes("M1 1AD", "M1 1FY");

                
                if (distance == null)
                {
                    return NotFound();
                }
                return Ok(distance);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
