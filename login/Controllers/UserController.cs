﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bll.manager.tradesman;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace login.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserManager _userManager;

        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        [Route("GetUsers")]
        public ActionResult GetUsers()
        {
            try
            {
                var data =  _userManager.GetUsers();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetUserById/{userid}")]
        public ActionResult GetUserById(int userId)
        {
            try
            {
                var data = _userManager.GetUserById(userId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetUserRules")]
        public ActionResult GetUserRules()
        {
            try
            {
                var data = _userManager.GetUserRules();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetUserRuleId/{ruleId}")]
        public ActionResult GetUserRuleId(int ruleId)
        {
            try
            {
                var data = _userManager.GetUserRuleById(ruleId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetUserTask")]
        public ActionResult GetUserTask()
        {
            try
            {
                var data = _userManager.GetUserTask();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetUserTaskById/{taskId}")]
        public ActionResult GetUserTaskById(int taskId)
        {
            try
            {
                var data = _userManager.GetUserTaskById(taskId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetUserRuleMapper")]
        public ActionResult GetUserRuleMapper()
        {
            try
            {
                var data = _userManager.GetUserRuleMapper();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetLeftNavigation")]
        public ActionResult GetLeftNavigation()
        {
            try
            {
                var data = _userManager.LeftNavigation();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
