﻿using System;
using System.Threading.Tasks;
using bll.dto;
using bll.manager.tradesman;
using Microsoft.AspNetCore.Mvc;

namespace login.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginManager _loginManager;

        public LoginController(ILoginManager loginManager)
        {
            _loginManager = loginManager;
        }

        [HttpPost]
        [Route("InsertItem")]
        public async Task<IActionResult> InsertItem([FromBody] UserDto userDto)
        {
            try
            {
                string tradesmenId = await _loginManager.InsertUserAsync(userDto);
                return Ok("User created successfully");
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        [HttpPost]
        [Route("InsertItemTradesman")]
        public async Task<IActionResult> InsertItemTradesman([FromBody] UserDto userDto, [FromQueryAttribute] UserProfileDto userProfileDto)
        {
            try
            {
                string tradesmenId = await _loginManager.InsertItemTradesman(userDto, userProfileDto);
                return Ok("Tradesman created successfully");
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        [HttpPost]
        [Route("GetUserByEmail")]
        public ActionResult GetUserByEmail([FromBody] UserDto userDto)
        {

            try
            {
                var data = _loginManager.GetUserByEmailAsync(userDto);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
