﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class TradeServiceDto
    {
        public int TradeServiceId { get; set; }
        public int CompanyProfileId { get; set; }
        public CompanyProfileDto CompanyProfile { get; set; }
        public int ItemCategoryId { get; set; }
        public ItemCategoryDto ItemCategory { get; set; }
        public string ServiceDescription { get; set; }
        public string CellNo { get; set; }
        public bool IsChecked { get; set; }
        public bool IsApproved { get; set; }
    }
}
