﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class UserDto
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool? Tradesman { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
