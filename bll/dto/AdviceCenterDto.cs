﻿using dal.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class AdviceCenterDto
    {
        public int AdviceCenterId { get; set; }
        public string Title { get; set; }
        public string PostText { get; set; }
        public string AuthorName { get; set; }
        public DateTime CreateDate { get; set; }
        public string PostImage { get; set; }
        public int UserProfileId { get; set; }
        //public UserProfileDto UserProfile { get; set; }
        public ICollection<AdviceCenterPostComment> AdviceCenterPostComment { get; set; }
    }
}
