﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class MenuDto
    {
        public int MenuId { get; set; }
        public string Title { get; set; }
        public string Translate { get; set; }
        public string Type { get; set; }
        public string Icon { get; set; }
        //public ICollection<SubMenu> Children { get; set; }
        public SubMenuDto Children { get; set; }
    }
}
