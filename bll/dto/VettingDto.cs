﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class VettingDto
    {
        public int VettingId { get; set; }
        public int TradeServiceId { get; set; }
        public TradeServiceDto TradeService { get; set; }
        public string VettingHead { get; set; }
        public string VettingValueofHead { get; set; }
        public int Status { get; set; }
        public string Comments { get; set; }
    }
}
