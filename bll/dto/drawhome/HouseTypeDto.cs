﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto.drawhome
{
   public class HouseTypeDto
    {
        public int HouseTypeId { get; set; }
        public string HouseTypeName { get; set; }
        public string FileName { get; set; }
    }
}
