﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto.drawhome
{
   public class KnowledgeBaseDto
    {
        public int KnowledgeBaseId { get; set; }
        public string WarningCode { get; set; }
        public string WarningText { get; set; }
        public string WarningUrl { get; set; }
        public string PermissionRequirements { get; set; }
        public string RefusedApplication { get; set; }
    }
}
