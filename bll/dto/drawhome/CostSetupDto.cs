﻿using dal.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto.drawhome
{
   public class CostSetupDto
    {
        public int CostSetupId { get; set; }
        public int MaterialId { get; set; }
        public Material Material { get; set; }
        public string Quantity { get; set; }
        public string Cost { get; set; }
    }
}
