﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class UserProfileDto
    {
        public int UserProfileId { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string PostCode { get; set; }
        public string ProfilePicture { get; set; }
        public string TradeLicence { get; set; }
        public string MobilNo { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
    }
}
