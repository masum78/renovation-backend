﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class MaterialDto
    {
        public int MaterialId { get; set; }
        public string MaterialName { get; set; }
    }
}
