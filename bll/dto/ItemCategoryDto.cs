﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class ItemCategoryDto
    {
        public int ItemCategoryId { get; set; }
        public string ItemCategoryName { get; set; }
        public int ParentId { get; set; }
    }
}
