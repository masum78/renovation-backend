﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
  public  class UserTaskDto
    {
        public int UserTaskId { get; set; }
        public string UserTaskName { get; set; }
    }
}
