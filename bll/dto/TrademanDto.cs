﻿using dal.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
  public  class TrademanDto
    {
        public int TradesmanId { get; set; }
        public string TradesmanName { get; set; }
        public int UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
        public int ItemCategoryId { get; set; }
        public ItemCategory ItemCategory { get; set; }
        public string TradeLicence { get; set; }
        public string PostCode { get; set; }





        public string Image { get; set; }
        public string Tags { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        //public string Features { get; set; }
        //public string* Rating { get; set; }
        //public string* NumberofSales { get; set; }
        public string LastUpdate { get; set; }
        //public string AddtoCart { get; set; }
        public string PersonsName { get; set; }
        public string PersonsAddress { get; set; }
        public string ServiceDescription { get; set; }
        public string MobilNo { get; set; }
        public string SendSms { get; set; }
        public string PromoCode { get; set; }
        public string CompanyName { get; set; }
        public string MemberSince { get; set; }
        public string CompanyLogo { get; set; }
        public string Distance { get; set; }
        public string Description { get; set; }
        public double Rate { get; set; }
        public bool Active { get; set; }
    }
}
