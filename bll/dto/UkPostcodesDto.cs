﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class UkPostcodesDto
    {
        public int PostcodeId { get; set; }
        public string pcd { get; set; }
        public string pcd2 { get; set; }
        public string pcds { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }

        public DateTime DateCreated
        {
            get
            {
                return this.dateCreated.HasValue
                   ? this.dateCreated.Value
                   : DateTime.Now;
            }

            set { this.dateCreated = value; }
        }

        private DateTime? dateCreated = null;
    }
}
