﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class UserRuleDto
    {
        public int UserRuleId { get; set; }
        public string UserRuleName { get; set; }
    }
}
