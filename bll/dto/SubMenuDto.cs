﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class SubMenuDto
    {
        public int SubMenuId { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public ICollection<MenuDto> Menu { get; set; }
    }
}
