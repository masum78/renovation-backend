﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bll.dto
{
   public class UserRuleMapperDto
    {
        public int UserRuleMapperId { get; set; }
        public int UserId { get; set; }
        public UserDto User { get; set; }
        public int UserRuleId { get; set; }
        public UserRuleDto UserRule { get; set; }
    }
}
