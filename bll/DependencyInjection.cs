﻿using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using System.Reflection;
using dal;
using bll.manager.tradesman;

namespace bll
{
   public static class DependencyInjection
    {
        public static IServiceCollection AddBusinessLayer(this IServiceCollection services, string connection)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddDataAccess(connection);

            services.AddScoped<ITradesmanManager, TradesmanManager>();
            services.AddScoped<IAdviceCenterManager, AdviceCenterManager>();
            services.AddScoped<ILoginManager, LoginManager>();
            services.AddScoped<IDrawHomeManager, DrawHomeManager>();
            services.AddScoped<IKnowledgeBaseManager, KnowledgeBaseManager>();
            services.AddScoped<IUserManager, UserManager>();
            return services;
        }
    }
}
