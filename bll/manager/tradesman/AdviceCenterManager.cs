﻿using AutoMapper;
using bll.dto;
using dal;
using dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bll.manager.tradesman
{
    public class AdviceCenterManager : IAdviceCenterManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AdviceCenterManager(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<AdviceCenterDto> AdviceCenterTitleList()
        {
            try
            {
                var data = _unitOfWork.AdviceCenter.AdviceCenterTitles();
                return _mapper.Map<List<AdviceCenter>, List <AdviceCenterDto>> (data.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AdviceCenterDto GetAdviceCenterPostByAdvicecenterId(int advicecenterId)
        {
            try
            {
                var data = _unitOfWork.AdviceCenter.GetAdviceCenterPostByAdvicecenterId(advicecenterId);
                return _mapper.Map<AdviceCenter, AdviceCenterDto>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AdviceCenterDto GetAdviceCenterPosts()
        {
            try
            {
                var data =  _unitOfWork.AdviceCenter.AdviceCenterPosts();
                return _mapper.Map<AdviceCenterDto>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<AdviceCenterPostCommentDto> GetCommentByAdviceCenterId(int advicecenterId)
        {
            try
            {
                var data = _unitOfWork.AdviceCenter.GetCommentByAdviceCenterId(advicecenterId);
                return _mapper.Map<List<AdviceCenterPostComment>, List<AdviceCenterPostCommentDto>>(data.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
