﻿using AutoMapper;
using bll.dto;
using dal;
using dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bll.manager.tradesman
{
   public class TradesmanManager : ITradesmanManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TradesmanManager(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public  Task<TrademanDto> GetTrademanAsync()
        {
            throw new NotImplementedException();
        }

        public Task<TrademanDto> GetTrademanByPostcode(string postcode)
        {
            throw new NotImplementedException();
        }

        
        public IEnumerable<TrademanDto> GetTradesmen()
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetTradesmen();
                var clientsDto = _mapper.Map<List<Tradesman>, List<TrademanDto>>(clients.ToList());
                // Returning only two columns rather than all to the controller end.
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TrademanDto GetItemUsingTradesmanId(int tradesmanId)
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetItemUsingTradesmanId(tradesmanId);
                var tradesmanDto = _mapper.Map<Tradesman, TrademanDto>(clients);
                return tradesmanDto;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<IEnumerable<TrademanDto>> GetTrademanListAsync()
        {
            try
            {
                var data = await _unitOfWork.Tradesman.GetAllAsync();
                return _mapper.Map<List<TrademanDto>>(data.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<string> PostTrademanAsync(TrademanDto poDto)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<TrademanDto>> GetItemsUsingPostcode(string postcode)
        {
            try
            {
                var clients = await _unitOfWork.Tradesman.GetItemsUsingPostcodeAsync(postcode);
                var clientsDto = _mapper.Map<List<Tradesman>, List<TrademanDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<TrademanDto> GetItemsUsingTradesmenName(string tradesmenName)
        {
            try
            {
                var clients = _unitOfWork.Tradesman.GetItemsUsingTradesmenName(tradesmenName);
                var clientsDto = _mapper.Map<List<Tradesman>, List<TrademanDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<TrademanDto> GetItemsUsingCompanyName(string companyName)
        {
            try
            {
                var clients = _unitOfWork.Tradesman.GetItemsUsingCompanyName(companyName);
                var clientsDto = _mapper.Map<List<Tradesman>, List<TrademanDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> InsertAsync(TrademanDto trademanDto)
        {
            try
            {
                Tradesman tradesman = _mapper.Map<TrademanDto, Tradesman>(trademanDto);
                var tradesmenId = await _unitOfWork.Tradesman.InsertItem(tradesman);

                return tradesmenId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> UpdateData(TrademanDto trademanDto)
        {
            try
            {
                
                var tradesman = await _unitOfWork.Tradesman
                    .FirstOrDefaultAsync(u => u.TradesmanId.Equals(trademanDto.TradesmanId));
                _mapper.Map(trademanDto, tradesman);
                return await _unitOfWork.CompleteAsync();
            }
            catch (Exception ex)
            {
                // throwing an exception if there's any.
                throw ex;
            }
        }

        public IEnumerable<Object> GetItemCategorys()
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetItemCategorys();
                return clients /*clientsDto*/;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<ItemCategoryDto> GetSubCategorys()
        {
            try
            {

                var data = _unitOfWork.Tradesman.GetSubCategorys();
                var clientsDto = _mapper.Map<List<ItemCategory>, List<ItemCategoryDto>>(data.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ItemCategoryDto GetSubCategoryById(int id)
        {
            try
            {

                var data = _unitOfWork.Tradesman.GetSubCategoryById(id);
                ItemCategoryDto itemCategoryDto = _mapper.Map<ItemCategory, ItemCategoryDto>(data);
                return itemCategoryDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public IEnumerable<Object> GetSearchItemCategorys()
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetSearchItemCategorys();
                
                return clients /*clientsDto*/;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void StorePostcods(List<Object> ukPostcods)
        {
            try
            {
                //var ukPostcodes = _mapper.Map<List<UkPostcodesDto>, List<UkPostcodes>>(ukPostcods.ToList());
                _unitOfWork.Tradesman.StorePostcods(ukPostcods);
                
                // var clientsDto = _mapper.Map<List<Tradesman>, List<TrademanDto>>(clients.ToList());
                // Returning only two columns rather than all to the controller end.
                // return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<TrademanDto> GetSearchItemCategorys(int selectedItemCategoryId, string postcode)
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetSearchItemCategorys(selectedItemCategoryId, postcode);
                var clientsDto = _mapper.Map<List<Tradesman>, List<TrademanDto>>(clients.ToList());
                // Returning only two columns rather than all to the controller end.
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ItemCategoryDto> GetAllCategory()
        {
            try
            {

                var data = _unitOfWork.Tradesman.GetAllCategory();
                IEnumerable<ItemCategoryDto> itemCategoryDto = _mapper.Map<List<ItemCategory>, List<ItemCategoryDto>>(data.ToList());
                return itemCategoryDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertItemSubCategory(ItemCategoryDto itemCategoryDto)
        {
            try
            {
                ItemCategory itemCategory = _mapper.Map<ItemCategoryDto, ItemCategory>(itemCategoryDto);
                _unitOfWork.Tradesman.InsertItemSubCategory(itemCategory);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CompanyProfileDto> GetCompanyProfile(int userId)
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetCompanyProfile(userId);
                var clientsDto = _mapper.Map<List<CompanyProfile>, List<CompanyProfileDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CompanyProfileDto GetCompanyProfileById(int companyProfileId, int userId)
        {
            try
            {
                var clients = _unitOfWork.Tradesman.GetCompanyProfileById(companyProfileId, userId);
                var clientsDto = _mapper.Map<CompanyProfile, CompanyProfileDto>(clients);
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> SaveTradeService(TradeServiceDto tradeServiceDto)
        {
            try
            {
                TradeService tradeService = _mapper.Map<TradeServiceDto, TradeService>(tradeServiceDto);
                var serviceDescription = await _unitOfWork.Tradesman.SaveTradeService(tradeService);

                return serviceDescription;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<TradeServiceDto> GetTradeService()
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetTradeService();
                var clientsDto = _mapper.Map<List<TradeService>, List<TradeServiceDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<VettingDto> GetVettingStatus(int tradeServiceId)
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetVettingStatus(tradeServiceId);
                var clientsDto = _mapper.Map<List<Vetting>, List<VettingDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<MenuDto> GetNavigation()
        {
            try
            {

                var clients = _unitOfWork.Tradesman.GetNavigation();
                var clientsDto = _mapper.Map<List<Menu1>, List<MenuDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
