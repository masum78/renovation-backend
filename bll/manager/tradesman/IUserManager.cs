﻿using bll.dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace bll.manager.tradesman
{
   public interface IUserManager
    {
        IEnumerable<UserDto> GetUsers();
        UserDto GetUserById(int userId);
        IEnumerable<UserRuleDto> GetUserRules();
        UserRuleDto GetUserRuleById(int ruleId);
        IEnumerable<UserTaskDto> GetUserTask();
        IEnumerable<UserRuleMapperDto> GetUserRuleMapper();
        UserTaskDto GetUserTaskById(int taskId);
        object LeftNavigation();
    }
}
