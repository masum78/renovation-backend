﻿using bll.dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace bll.manager.tradesman
{
   public interface ITradesmanManager
    {
        Task<IEnumerable<TrademanDto>> GetTrademanListAsync();
        Task<TrademanDto> GetTrademanAsync();
        Task<string> PostTrademanAsync(TrademanDto poDto);

        Task<TrademanDto> GetTrademanByPostcode(string postcode);


        // New 11 July 2020
        IEnumerable<TrademanDto> GetTradesmen();

        //IEnumerable<ItemCategoryDto> GetItemCategorys(); GetSearchItemCategorys

        IEnumerable<Object> GetSearchItemCategorys();
        IEnumerable<MenuDto> GetNavigation();
        IEnumerable<Object> GetItemCategorys();
        IEnumerable<ItemCategoryDto> GetSubCategorys();

        ItemCategoryDto GetSubCategoryById(int id);
        IEnumerable<ItemCategoryDto> GetAllCategory();
        void InsertItemSubCategory(ItemCategoryDto itemCategoryDto);
        IEnumerable<TrademanDto> GetSearchItemCategorys(int selectedItemCategoryId, string postcode);
        TrademanDto GetItemUsingTradesmanId(int tradesmanId);

        Task<IEnumerable<TrademanDto>> GetItemsUsingPostcode(string postcode);

        IEnumerable<TrademanDto> GetItemsUsingTradesmenName(string tradesmenName);
        IEnumerable<TrademanDto> GetItemsUsingCompanyName(string companyName);
        IEnumerable<CompanyProfileDto> GetCompanyProfile(int userId);
        CompanyProfileDto GetCompanyProfileById(int companyProfileId, int userId);
        Task<string> InsertAsync(TrademanDto trademanDto);

        IEnumerable<TradeServiceDto> GetTradeService();
        IEnumerable<VettingDto> GetVettingStatus(int tradeServiceId);
        Task<string> SaveTradeService(TradeServiceDto tradeServiceDto);

        public void StorePostcods(List<Object> ukPostcods);

        Task<int> UpdateData(TrademanDto trademanDto);
    }
}
