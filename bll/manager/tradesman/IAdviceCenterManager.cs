﻿
using bll.dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace bll.manager.tradesman
{
   public interface IAdviceCenterManager
    {
        AdviceCenterDto GetAdviceCenterPosts();
        IEnumerable<AdviceCenterDto> AdviceCenterTitleList();
        AdviceCenterDto GetAdviceCenterPostByAdvicecenterId(int advicecenterId);

        IEnumerable<AdviceCenterPostCommentDto> GetCommentByAdviceCenterId(int advicecenterId);
    }
}
