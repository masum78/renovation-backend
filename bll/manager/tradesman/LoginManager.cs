﻿using AutoMapper;
using bll.dto;
using dal;
using dal.models;
using System;
using System.Threading.Tasks;

namespace bll.manager.tradesman
{
    public class LoginManager : ILoginManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LoginManager(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public UserDto GetUserByEmailAsync(UserDto userDto)
        {
            try
            {
                //var client =  _unitOfWork.Login.GetUserByEmail(userDto);
                var client = _unitOfWork.Login.GetUserByEmail(_mapper.Map<UserDto, User>(userDto));
                var clientsDto = _mapper.Map<User, UserDto>(client);
                return clientsDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task InactiveUserAsync(UserDto userDto)
        {
            throw new NotImplementedException();
        }

        public async Task<string> InsertItemTradesman(UserDto userDto, UserProfileDto userProfileDto)
        {
            try
            {

                User user = _mapper.Map<UserDto, User>(userDto);
                UserProfile userProfile = _mapper.Map<UserProfileDto, UserProfile>(userProfileDto);

                var tradesmenId = await _unitOfWork.Login.InsertItemTradesman(user, userProfile);

                return tradesmenId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> InsertUserAsync(UserDto userDto)
        {
            try
            {
               
                User user = _mapper.Map<UserDto, User>(userDto);

                var tradesmenId = await _unitOfWork.Login.InsertItem(user);

                return tradesmenId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<bool> IsUserExistsAsync(UserDto userDto)
        {
            throw new NotImplementedException();
        }
    }
}
