﻿using AutoMapper;
using bll.dto;
using bll.dto.drawhome;
using dal;
using dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bll.manager.tradesman
{
    public class DrawHomeManager : IDrawHomeManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public DrawHomeManager(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IEnumerable<HouseTypeDto> GetHouseTypes()
        {
            try
            {
                var data = _unitOfWork.DrawHome.GetHouseTypes();
                return _mapper.Map<List<HouseType>, List<HouseTypeDto>>(data.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<HouseTypeLayoutDto> GetLayouts()
        {
            try
            {
                var data = _unitOfWork.DrawHome.GetLayouts();
                return _mapper.Map<List<HouseTypeLayout>, List<HouseTypeLayoutDto>>(data.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CostSetupDto> GetCostSetupData()
        {
            try
            {
                var data = _unitOfWork.DrawHome.GetCostSetupData();
                return _mapper.Map<List<CostSetup>, List<CostSetupDto>>(data.ToList());


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ProductDto> GetProducts(int materialId)
        {
            try
            {
                var data = _unitOfWork.DrawHome.GetProducts(materialId);
                return _mapper.Map<List<Product>, List<ProductDto>>(data.ToList());


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<MaterialDto> GetMaterials()
        {
            try
            {
                var data = _unitOfWork.DrawHome.GetMaterials();
                return _mapper.Map<List<Material>, List<MaterialDto>>(data.ToList());


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<MaterialDto> GetMaterialById(int materialId)
        {
            try
            {
                var data = _unitOfWork.DrawHome.GetMaterialById(materialId);
                return _mapper.Map<List<Material>, List<MaterialDto>>(data.ToList());


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<HouseTypeLayoutDto> StoreLayout(HouseTypeLayoutDto houseTypeLayoutDto, int userId)
        {
            try
            {
                HouseTypeLayout houseTypeLayout = _mapper.Map<HouseTypeLayoutDto, HouseTypeLayout>(houseTypeLayoutDto);

                var layouts = _unitOfWork.DrawHome.StoreLayout(houseTypeLayout, userId);

                IEnumerable<HouseTypeLayoutDto> layoutList = _mapper.Map<List<HouseTypeLayout>, List<HouseTypeLayoutDto>>(layouts.ToList());

                return layoutList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
    }
}
