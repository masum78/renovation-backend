﻿using AutoMapper;
using bll.dto;
using dal;
using dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bll.manager.tradesman
{
    public class UserManager : IUserManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserManager(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public UserDto GetUserById(int userId)
        {
            try
            {

                var client = _unitOfWork.User.GetUserById(userId);
                var clientsDto = _mapper.Map<User, UserDto>(client);
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserRuleDto GetUserRuleById(int ruleId)
        {
            try
            {

                var client = _unitOfWork.User.GetUserRuleById(ruleId);
                var clientsDto = _mapper.Map<UserRule, UserRuleDto>(client);
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<UserRuleMapperDto> GetUserRuleMapper()
        {
            try
            {

                var clients = _unitOfWork.User.GetUserRuleMapper();
                var clientsDto = _mapper.Map<List<UserRuleMapper>, List<UserRuleMapperDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<UserRuleDto> GetUserRules()
        {
            try
            {

                var clients = _unitOfWork.User.GetUserRules();
                var clientsDto = _mapper.Map<List<UserRule>, List<UserRuleDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
                
        public IEnumerable<UserDto> GetUsers()
        {
            try
            {

                var clients = _unitOfWork.User.GetUsers();
                var clientsDto = _mapper.Map<List<User>, List<UserDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<UserTaskDto> GetUserTask()
        {
            try
            {

                var clients = _unitOfWork.User.GetUserTask();
                var clientsDto = _mapper.Map<List<UserTask>, List<UserTaskDto>>(clients.ToList());
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserTaskDto GetUserTaskById(int taskId)
        {
            try
            {

                var client = _unitOfWork.User.GetUserTaskById(taskId);
                var clientsDto = _mapper.Map<UserTask, UserTaskDto>(client);
                return clientsDto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object LeftNavigation()
        {
            try
            {

                var client = _unitOfWork.User.LeftNavigation();
                return client;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
