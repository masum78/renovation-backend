﻿using AutoMapper;
using bll.dto.drawhome;
using dal;
using dal.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace bll.manager.tradesman
{
    public class KnowledgeBaseManager : IKnowledgeBaseManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public KnowledgeBaseManager(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IEnumerable<KnowledgeBaseDto> GetKnowledgeBaseData()
        {
            try
            {
                var data = _unitOfWork.KnowledgeBaseRepository.GetKnowledgeBaseData();
                return _mapper.Map<List<KnowledgeBase>, List<KnowledgeBaseDto>>(data.ToList());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
