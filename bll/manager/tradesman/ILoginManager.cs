﻿

using bll.dto;
using System.Threading.Tasks;

namespace bll.manager.tradesman
{
   public interface ILoginManager
    {
        
        Task<string> InsertUserAsync(UserDto userDto);
        Task<string> InsertItemTradesman(UserDto userDto, UserProfileDto userProfileDto);
        Task<bool> IsUserExistsAsync(UserDto userDto);
        Task InactiveUserAsync(UserDto userDto);
        UserDto GetUserByEmailAsync(UserDto userDto);
    }
}
