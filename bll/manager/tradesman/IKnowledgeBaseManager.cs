﻿using bll.dto.drawhome;
using System;
using System.Collections.Generic;
using System.Text;

namespace bll.manager.tradesman
{
   public interface IKnowledgeBaseManager
    {
        IEnumerable<KnowledgeBaseDto> GetKnowledgeBaseData();
    }
}
