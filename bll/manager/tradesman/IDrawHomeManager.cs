﻿using bll.dto;
using bll.dto.drawhome;
using System;
using System.Collections.Generic;
using System.Text;

namespace bll.manager.tradesman
{
    public interface IDrawHomeManager
    {
        IEnumerable<HouseTypeDto> GetHouseTypes();
        IEnumerable<HouseTypeLayoutDto> StoreLayout(HouseTypeLayoutDto houseTypeLayoutDto, int userId);
        IEnumerable<HouseTypeLayoutDto> GetLayouts();

        IEnumerable<ProductDto> GetProducts(int materialId);
        IEnumerable<MaterialDto> GetMaterials();
        IEnumerable<MaterialDto> GetMaterialById(int materialId);
        IEnumerable<CostSetupDto> GetCostSetupData();


        
    }
}
