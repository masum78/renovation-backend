﻿using AutoMapper;
using bll.dto;
using bll.dto.drawhome;
using dal.models;

namespace bll
{
    class MapperProfile : Profile
    {
        public MapperProfile()
        {
            // Map Domain model to Dto
            CreateMap<Tradesman, TrademanDto>();
            // Map Dto to domain
            CreateMap<TrademanDto, Tradesman>();

            // Map Domain model to Dto
            CreateMap<AdviceCenter, AdviceCenterDto>();
             // Map Dto to domain
            CreateMap<AdviceCenterDto, AdviceCenter>();

            // Map Domain model to Dto
            CreateMap<User, UserDto>();
            // Map Dto to domain
            CreateMap<UserDto, User>();

            // Map Domain model to Dto
            CreateMap<ItemCategory, ItemCategoryDto>();
            // Map Dto to domain
            CreateMap<ItemCategoryDto, ItemCategory>();

            // Map Domain model to Dto
            CreateMap<HouseType, HouseTypeDto>();
            // Map Dto to domain
            CreateMap<HouseTypeDto, HouseType>();

            // Map Domain model to Dto
            CreateMap<HouseTypeLayout, HouseTypeLayoutDto>();
            // Map Dto to domain
            CreateMap<HouseTypeLayoutDto, HouseTypeLayout>();

            // Map Domain model to Dto
            CreateMap<Material, MaterialDto>();
            // Map Dto to domain
            CreateMap<MaterialDto, Material>();

            // Map Domain model to Dto
            CreateMap<Product, ProductDto>();
            // Map Dto to domain
            CreateMap<ProductDto, Product>();

            // Map Domain model to Dto
            CreateMap<CostSetup, CostSetupDto>();
            // Map Dto to domain
            CreateMap<CostSetupDto, CostSetup>();

            // Map Domain model to Dto
            CreateMap<KnowledgeBase, KnowledgeBaseDto>();
            // Map Dto to domain
            CreateMap<KnowledgeBaseDto, KnowledgeBase>();

            // Map Domain model to Dto
            CreateMap<CompanyProfile, CompanyProfileDto>();
            // Map Dto to domain
            CreateMap<CompanyProfileDto, CompanyProfile>();

            // Map Domain model to Dto
            CreateMap<TradeService, TradeServiceDto>();
            // Map Dto to domain
            CreateMap<TradeServiceDto, TradeService>();

            // Map Domain model to Dto
            CreateMap<Vetting, VettingDto>();
            // Map Dto to domain
            CreateMap<VettingDto, Vetting>();

            // Map Domain model to Dto
            CreateMap<Menu1, MenuDto>();
            // Map Dto to domain
            CreateMap<MenuDto, Menu1>();

            // Map Domain model to Dto
            CreateMap<SubMenu1, SubMenuDto>();
            // Map Dto to domain
            CreateMap<SubMenuDto, SubMenu1>();

            // Map Domain model to Dto
            CreateMap<UserRule, UserRuleDto>();
            // Map Dto to domain
            CreateMap<UserRuleDto, UserRule>();

            // Map Domain model to Dto
            CreateMap<UserTask, UserTaskDto>();
            // Map Dto to domain
            CreateMap<UserTaskDto, UserTask>();


            // Map Domain model to Dto
            CreateMap<UserRuleMapper, UserRuleMapperDto>();
            // Map Dto to domain
            CreateMap<UserRuleMapperDto, UserRuleMapper>();

        }

    }
}
