﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bll.manager.tradesman;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace drawhome.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class KnowledgeBaseController : ControllerBase
    {
        private readonly IKnowledgeBaseManager _knowledgeBaseManager;
        public KnowledgeBaseController(IKnowledgeBaseManager knowledgeBaseManager)
        {
            _knowledgeBaseManager = knowledgeBaseManager;
        }

        [HttpGet]
        [Route("GetKnowledgeBaseData")]
        public ActionResult GetKnowledgeBaseData()
        {

            try
            {
                var data = _knowledgeBaseManager.GetKnowledgeBaseData();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
