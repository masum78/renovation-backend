﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bll.dto.drawhome;
using bll.manager.tradesman;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace drawhome.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DrawHomeController : ControllerBase
    {
        private readonly IDrawHomeManager _drawHomeManager;

        public DrawHomeController(IDrawHomeManager drawHomeManager)
        {
            _drawHomeManager = drawHomeManager;
        }

        [HttpGet]
        [Route("GetHouseTypes")]
        public ActionResult GetHouseTypes()
        {

            try
            {
                var data = _drawHomeManager.GetHouseTypes();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpGet]
        [Route("GetLayouts")]
        public ActionResult GetLayouts()
        {

            try
            {
                var data = _drawHomeManager.GetLayouts();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpGet]
        [Route("GetSuppliers/{materialId}")]
        public ActionResult GetSuppliers(int materialId)
        {

            try
            {
                var data = _drawHomeManager.GetProducts(materialId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetMaterials")]
        public ActionResult GetMaterials()
        {

            try
            {
                var data = _drawHomeManager.GetMaterials();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetMaterialById/{materialId}")]
        public ActionResult GetMaterialById(int materialId)
        {

            try
            {
                var data = _drawHomeManager.GetMaterialById(materialId);
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("GetCostSetupData")]
        public ActionResult GetCostSetupData()
        {

            try
            {
                var data = _drawHomeManager.GetCostSetupData();
                if (data == null)
                {
                    return NotFound();
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [Route("SaveLayout")]
        public IActionResult SaveLayout([FromBody] HouseTypeLayoutDto houseTypeLayoutDto)
        {
            try
            {

                IEnumerable<HouseTypeLayoutDto> houseTypeLayoutDtos = _drawHomeManager.StoreLayout(houseTypeLayoutDto, 4);
                //string tradesmenId = await _tradesmanManager.InsertAsync(trademanDto);
                return Ok(houseTypeLayoutDtos);
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        
    }
}
